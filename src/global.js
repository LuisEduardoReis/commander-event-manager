const DEBUG = false

// State
let STATE = {
    players: [],
    rounds: [],
    roundResults: []
}

// UI Components
let UI = {}

// Pods
const DEFAULT_PODS = []
DEFAULT_PODS[8] =  ["4 4", "4 4"]
DEFAULT_PODS[9] =  ["3 3 3", "3 3 3"]
DEFAULT_PODS[10] = ["4 3 3", "3 3 4"]
DEFAULT_PODS[11] = ["4 4 3", "3 4 4"]
DEFAULT_PODS[12] = ["4 4 4", "3 3 3 3"]
DEFAULT_PODS[13] = ["4 3 3 3", "4 3 3 3"]
DEFAULT_PODS[14] = ["4 4 3 3", "4 4 3 3"]
DEFAULT_PODS[15] = ["4 4 4 3", "4 4 4 3"]
DEFAULT_PODS[16] = ["4 4 4 4", "4 4 4 4"]
