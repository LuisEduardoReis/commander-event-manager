// Setup            
//window.onbeforeunload = (() => true)
function setup() {
    setupDisplay()
    setupStartUI()
}

// Start the Tournament
function start() {
    // Create player list
    let playerNames = parsePlayerNames(UI.display.value())
    playerNames = uniq(playerNames)

    STATE.players = playerNames.map((name, i) => { return {
        index: i,
        name: name,
        points: 0,
        tableTieBreaker: 0,
        entropy: Math.random()
    }})

    // Remove Start button and disable the display
    UI.startButton.remove()
    UI.display.elt.disabled = true

    // Generate Round
    generateRound()
}

// Generate a new Round
function generateRound(roundString) {
    // Copy player array and sort by leaderboard
    let roundPlayers = Array.from(STATE.players)
        .sort(playerComparator)
        .map(p => p.index)

    // Split the sorted list into pods
    const round = []
    const podsString = validPodString(roundString) ? roundString : generatePodsString(roundPlayers.length, STATE.rounds.length)
    const pods = parsePodString(podsString)
    
    pods.forEach(podSize => {
        round.push(roundPlayers.splice(0, podSize))                    
    })

    // Create the round, update the display and move to the result view
    STATE.rounds.push(round)
    
    setupRoundUI()
}

// Go back from a round
function backFromRound() {
    if (!confirm(LABELS.discardRound)) return

    UI.resultsDiv.remove()
    if (STATE.rounds.length <= 1) {
        STATE.rounds = []
        STATE.roundResults = []
        setupStartUI()            
    } else {
        STATE.rounds.pop()
        setupStandingsUI()
    }
}

// Finish a round
function endRound() {
    // Remove the round results inputs
    UI.resultsDiv.remove()

    const roundResults = STATE.players.map(p => {
        return {index: p.index, result:0, pointDiff:0, table:-1}
    })

    // For each table
    UI.selects.forEach((tableSelects, tableIndex) => {
        let pointsForTheTable = [4,3,2,1]

        // Sort player by result
        let sortedResults = tableSelects
            .map(sel => { return {
                playerIndex: sel.playerIndex, 
                playerPosition: sel.selected()
            }})
            .filter(p => p.playerPosition != ' ')
            .sort((a,b) => a.playerPosition - b.playerPosition)

        // Store player result and table
        sortedResults.forEach(r => {
            roundResults[r.playerIndex].result = r.playerPosition
            roundResults[r.playerIndex].table = tableIndex
        })
        
        // Split table points between the players, spliting when needed
        // Keep going, don't look at this. It be ugly
        let pointsToSplit = 0
        let playersSpliting = []
        while(sortedResults.length > 0) {
            pointsToSplit += pointsForTheTable.shift()
            playersSpliting.push(sortedResults.shift())
            
            if (sortedResults.length == 0 || 
                playersSpliting[0].playerPosition != sortedResults[0].playerPosition) {

                // Update player points and tie breakers
                const splitPoints = pointsToSplit / playersSpliting.length
                playersSpliting.forEach(p => {
                    STATE.players[p.playerIndex].points += splitPoints
                    STATE.players[p.playerIndex].tableTieBreaker += STATE.rounds.length > 1 ? tableIndex+1 : 0
                    roundResults[p.playerIndex].pointDiff = splitPoints
                })

                pointsToSplit = 0
                playersSpliting = []
            }
        }
    })
    STATE.roundResults.push(roundResults)

    // Show standings
    setupStandingsUI()                
}

// Standings
function nextRound() {
    UI.standingsDiv.remove()
    generateRound(UI.nextRoundPodsInput.value())    
}

function backFromStandings() {
    UI.standingsDiv.remove()
    const results = STATE.roundResults.pop()
    results.forEach(r => {
        STATE.players[r.index].points -= r.pointDiff
    })
    
    setupRoundUI()
    
    UI.selects.flat().forEach(s => {
        s.selected(results[s.playerIndex].result)
    })

    checkResultsFilled()
}
