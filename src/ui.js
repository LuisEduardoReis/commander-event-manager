
///////// DISPLAY ////////
function setupDisplay() {
    createElement('h3').html(LABELS.header).position(10,-10)

    UI.mainDiv = createDiv()
    UI.mainDiv.position(10,40)             

    UI.display = createElement('textarea')
    UI.display.parent(UI.mainDiv)
    if (DEBUG) UI.display.value("P1\nP2\nP3\nP4\nP5\nP6\nP7\nP8\n")
    UI.display.elt.placeholder = LABELS.playerNamesPlaceholder
    UI.display.elt.cols = 32
    UI.display.elt.rows = 16
    
    createP().parent(UI.mainDiv)
}

//////// START BUTTON ///////
function setupStartUI() {
    UI.display.elt.disabled = false
    if (STATE.players.length != 0) {
        UI.display.value(STATE.players.map(p => p.name).join('\n'))
    }

    UI.startButton = createButton(LABELS.startTournament)
    UI.startButton.parent(UI.mainDiv)
    UI.startButton.mouseReleased(start)
    UI.startButton.elt.disabled = !DEBUG
    UI.display.input(() => {
        const numPlayers = parsePlayerNames(UI.display.value()).length
        UI.startButton.elt.disabled =  numPlayers < 8
    })
}

////////// ROUND /////////
function setupRoundUI() {
    displayRound()

    UI.selects = []
    UI.resultsDiv = createDiv()
    UI.resultsDiv.parent(UI.mainDiv)

    const round = STATE.rounds[STATE.rounds.length-1]
    round.forEach((table, index) => {
        tableHeader = createElement('h4')
        tableHeader.parent(UI.resultsDiv)
        tableHeader.html(LABELS.tableName(index))

        let tableSelects = []
        table.forEach((playerIndex, i) => {
            playerDiv = createDiv().parent(UI.resultsDiv)

            createElement('span')
                .parent(UI.resultsDiv)
                .html(`- ${STATE.players[playerIndex].name}`)

            resultSelect = createSelect()
            resultSelect.parent(UI.resultsDiv)
            resultSelect.elt.style.position = 'absolute'
            resultSelect.elt.style.right = 0
            const options = [' ', 1,2,3,4]
            options.forEach(o => resultSelect.option(o))
            resultSelect.selected(0)
            if (DEBUG) resultSelect.selected(1+Math.floor(4 * Math.random()))
            resultSelect.playerIndex = playerIndex
            resultSelect.input(checkResultsFilled)

            tableSelects.push(resultSelect)
        })

        UI.selects.push(tableSelects)
    })
    createP().parent(UI.resultsDiv)

    UI.endRoundButton = createButton(LABELS.endRound)
    UI.endRoundButton.parent(UI.resultsDiv)
    UI.endRoundButton.mouseReleased(endRound)
    checkResultsFilled()

    UI.returnButton = createButton(LABELS.back)
    UI.returnButton.parent(UI.resultsDiv)
    UI.returnButton.mouseReleased(backFromRound)
    
    createP().parent(UI.resultsDiv)
}
function checkResultsFilled() {
    const selectsUnfilled = UI.selects
        .flat()
        .filter(s => s.value() == ' ')
        .length > 0
    UI.endRoundButton.elt.disabled = selectsUnfilled                    
}
function displayRound() {
    const round = STATE.rounds[STATE.rounds.length-1]

    // Header
    let text = LABELS.round(STATE.rounds.length)    
    
    // List of tables
    round.forEach((table, index) => {
        text += LABELS.tableName(index) + '\n'
        text += table
            .map(playerIndex => STATE.players[playerIndex])
            .map(player => `- ${player.name} - ${player.points}`)
            .join('\n')
        text += '\n\n'
    })
    
    UI.display.value(text)
}

////// STANDINGS //////
function setupStandingsUI() {
    displayStandings()

    // Standings div
    UI.standingsDiv = createDiv().parent(UI.mainDiv)

    // Next round pods input
    createElement('span').parent(UI.standingsDiv).html(LABELS.nextRoundTables)
    UI.nextRoundPodsInput = createInput()
    UI.nextRoundPodsInput.parent(UI.standingsDiv)
    UI.nextRoundPodsInput.value(generatePodsString(STATE.players.length, STATE.rounds.length))
    UI.nextRoundPodsInput.input(() => {
        if (validPodString(UI.nextRoundPodsInput.value())) {
            UI.nextRoundPodsInput.style('color:black')
        } else {
            UI.nextRoundPodsInput.style('color:red')
        }
    })

    // Next round button
    createP().parent(UI.standingsDiv)
    UI.nextRoundButton = createButton(LABELS.nextRound)
    UI.nextRoundButton.parent(UI.standingsDiv)
    UI.nextRoundButton.mouseReleased(nextRound)

    // Back button
    UI.returnButton = createButton(LABELS.back)
    UI.returnButton.parent(UI.standingsDiv)
    UI.returnButton.mouseReleased(backFromStandings)
}

function displayStandings() {
    const standings = Array.from(STATE.players).sort(playerComparator)
    const lastRoundResults = STATE.roundResults[STATE.roundResults.length-1]
    const text = LABELS.roundStandings(STATE.rounds.length) + 
        standings.map((player, i) => {
                let tab = i < 9 ? ' ' : ''
                if (STATE.rounds.length < 2) {
                    return `${i+1}º${tab} ${player.points} pt - ${player.name}`
                } else {
                    return `${i+1}º${tab} ${player.points} pt (tb: ${player.tableTieBreaker}) - ${player.name}`
                }
            })
            .join('\n')

    UI.display.value(text) 
}