const LABELS = {
    header: "Gestor Eventos Commander",
    playerNamesPlaceholder: "Lista de jogadores:\n\nJogador 1\nJogador 2\nJogador 3\nJogador 4\n",
    startTournament: "Começar Torneio",
    endRound: "Terminar Ronda",
    nextRound: "Próxima Ronda",
    back: "Retroceder",
    nextRoundTables: "Mesas para a próxima ronda: ",
    discardRound: "Descartar ronda?",
    tableName: (i) => `Mesa ${i+1}`,
    round: (n) => `Ronda ${n}\n\n`,
    roundStandings: (n) => `Standings Ronda ${STATE.rounds.length}\n\n`
}