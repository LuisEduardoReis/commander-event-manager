 // Util
function shuffleArray(array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1))
        var temp = array[i]
        array[i] = array[j]
        array[j] = temp
    }
}
function parsePlayerNames(text) {
    return uniq(
        text.split('\n')
        .map(n => n.trim())
        .filter(p => p != '')
    )                
}
function generatePodsString(n, round) {
    if (DEFAULT_PODS[n] && DEFAULT_PODS[n][round]) {
        pods = DEFAULT_PODS[n][round]
        return pods
    } else {
        // Overfill with pods of 4
        let fours = Math.ceil(n / 4)
        // Reduce to pods of 3 until the number of player is correct
        let threes = 4*fours - n
        fours -= threes
        // Build pod string
        return [...Array(fours)].map(e => '4')
            .concat([...Array(threes)].map(e => '3'))
            .join(' ')
    }
}
function parsePodString(roundString) {
    roundString = roundString || ""
    return roundString.split('')
        .filter(e => e == '4' || e == '3')
        .map(e => Number.parseInt(e))
}
function validPodString(podString) {
    return parsePodString(podString)
        .concat(0)
        .reduce((a,b) => a+b) == STATE.players.length
}
function playerComparator(a,b) {
    if (a.points != b.points) return b.points - a.points
    else if (a.tableTieBreaker != b.tableTieBreaker) return a.tableTieBreaker - b.tableTieBreaker
    else return (a.entropy - b.entropy)
}
function uniq(a) {
    return a.sort().filter((item, pos, ary) => {
        return !pos || item != ary[pos - 1];
    });
}
